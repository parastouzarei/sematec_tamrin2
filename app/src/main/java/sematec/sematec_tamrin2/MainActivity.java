package sematec.sematec_tamrin2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends baseactivity implements View.OnClickListener {
    Button startsecondactivity;

    EditText enterName, enterFamily, enterAge;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button startsecondactivity=(Button)findViewById(R.id.startsecondactivity);
        startsecondactivity.setOnClickListener(this);

        enterName = (EditText) findViewById(R.id.enter_name);
        enterFamily = (EditText) findViewById(R.id.enter_family);
        enterAge = (EditText) findViewById(R.id.enter_age);



    }

    @Override
    public void onClick(View view) {
        if (view.getId()== R.id.startsecondactivity){
            Intent secondActivityintent= new Intent(mcontext,secondActivity.class);
//            secondActivityintent.putExtra("name","amirhosein");
//            secondActivityintent.putExtra("family","teimori");
//            secondActivityintent.putExtra("age","40");
            secondActivityintent.putExtra("name",enterName.getText().toString());
            secondActivityintent.putExtra("family", enterFamily.getText().toString());
            secondActivityintent.putExtra("age", enterAge.getText().toString());
            startActivity(secondActivityintent);


        }
    }
}